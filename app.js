// const express = require('express');
import express from 'express';
const app = express();
const port = process.env.PORT || 5000;
// import path from 'path'
// import { fileURLToPath } from 'url';
import cors from 'cors';
import mongoose from "mongoose";
// import {router} from "./database/routes/routes.js";
import {totalRoutes} from "./database/routes/index.js";
import proxy from 'express-http-proxy';

mongoose.Promise = global.Promise;

// const __filename = fileURLToPath(import.meta.url);
// const __dirname = path.dirname(__filename);

// const whitelist = ['https://mountainhug.com', 'http://localhost:4200', 'http://localhost:4202', 'http://localhost:5000']

const corsOpts = {
  origin:  "*",
  // origin: (origin, callback) => {
  //   if (whitelist.indexOf(origin) !== -1) {
  //     callback(null, true)
  //   } else {
  //     callback(new Error())
  //   }
  // },
  methods: "GET,HEAD,PUT,PATCH,DELETE",
  preflightContinue: false,
  optionsSuccessStatus: 200
}

const url = process.env.NODE_ENV === "local" ? process.env.MONGODB_LOCAL_URI : process.env.MONGODB_PRODUCTION_URI;

mongoose.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
// const database = mongoose.connection;

// database.on('error', (error) => {
//   console.log(error)
// })

// database.once('connected', () => {
//   console.log('Database Connected');
// })

app.use(cors(corsOpts))

// let DB = require('./database/mongoose');


app.use(express.static('public'))
app.use(express.json())
// app.use('/',proxy('http://localhost:'+port));
// app.use('/holiday',proxy('http://localhost:'+port+'/holiday'));


totalRoutes.forEach(route => {
  app.use('/api', route)
})


app.listen(port, () => console.log("Server Connected on port 5000"));






// module.exports = app;
// mongodb+srv://lynnchen:mlab1020@clusterly.ik2jvzc.mongodb.net/?retryWrites=true&w=majority