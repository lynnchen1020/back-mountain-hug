import mongoose from 'mongoose';

const IHoliday = new mongoose.Schema({
  holiday_ts: { type: Number, unique: true, required: true },
  alias: { type: String },
  created_time: { type: Number }
})

export default IHoliday