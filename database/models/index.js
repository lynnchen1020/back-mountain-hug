import mongoose from 'mongoose';
import IHoliday from './holiday.js';

const IBaseOrder = new mongoose.Schema({
  order_id: { type: String },
  area: { type: String },
  name: { type: String },
  phone: { type: String },
  email: { type: String },
  start_date: { type: String },
  end_date: { type: String },
  timestamp_start_date: { type: Number},
  tents_per_day: { type: [Number] },
  nights: { type: Number },
  prepaid_fee: { type: Number },
  is_prepaid: { type: Boolean },
  rest_fee: { type: Number },
  is_restpaid: { type: Boolean },
  total_fee: { type: Number },
  order_source: { type: String },
  is_canceled_order: { type: Boolean },
  event_id: { type: String },
  tents: { type: Number },
  is_night_going: { type: Boolean },
  created_time: { type: Number },
})

const ICampOrderDetails = new mongoose.Schema({
  tent_area: { type: String },
  tent_amount: { type: Number }
},{_id: false})

const ICampOrder = new mongoose.Schema({
  ...IBaseOrder.obj,
  order_details: ICampOrderDetails,
});

const IInnOrderDetails = new mongoose.Schema({
  adults: { type: Number },
  children: { type: Number },
  add_bed: { type: Number },
  add_breakfast: { type: Number },
  request: { type: String }
},{_id: false})

const IInnOrder = new mongoose.Schema({
  ...IBaseOrder.obj,
  adults: { type: Number },
  children: { type: Number },
  add_bed: { type: Number },
  add_breakfast: { type: Number }
});

// model('campOrder') => DB Name
const Model = {
  camp_a: mongoose.model('camp_a_orders', ICampOrder),
  camp_b: mongoose.model('camp_b_orders', ICampOrder),
  camp_c: mongoose.model('camp_c_orders', ICampOrder),
  camp_d: mongoose.model('camp_d_orders', ICampOrder),
  inn: mongoose.model('inn_orders', IInnOrder),
  holiday: mongoose.model('holidays', IHoliday),
}
export default Model;

// 'timestampUID',
// 'order_id',
// 'name',
// 'campArea',
// 'mobile',
// 'email',
// 'startDate', v
// 'endDate', v
// 'nights',
// 'tents',
// 'five_digits',
// 'prepaid_fee',
// 'is_paid_pre_fee',
// 'rest_fee',
// 'is_paid_rest_fee',
// 'total_fee',
// 'order_source',
// 'is_cancel_order'

// 時間戳記v
// 訂單編號v
// 姓名v
// 營位v
// 訂金
// 是否已付訂金
// 訂金匯款確認日期
// 聯絡手機
// Email
// 入住日期
// 離開日期
// 過夜日期
// 過夜天數
// 帳數
// 尾款
// 是否結清尾款
// 總費用
// 訂單類別
// 是否取消訂單
// 刪除日曆活動
// 重新預約日曆日期
// 寄送匯款確認信
// 是否夜衝
// 訂單備註