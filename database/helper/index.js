export function getCurrentDay(dateTime) {
  const dayMapping = {
    "0": '日',
    "1": '一',
    "2": '二',
    "3": '三',
    "4": '四',
    "5": '五',
    "6": '六',
  }

  return dayMapping[String(new Date(dateTime).getDay())];
}

// 為減輕查詢效能，前端顯示前 3 個月 Orders
export function getHalfYearOrder() {
  let from3MonthsAgo = '';
  const monthArray = Array.from({ length: 12 }, (_, index) => index + 1);

  const nowTime = new Date().toLocaleDateString('zh-TW').split('/')
  let nowYear = Number(nowTime[0]);
  let nowMonth = Number(nowTime[1]);
  const monthGap = 3;

  if(nowMonth > 3) {
    nowMonth = monthArray[nowMonth - monthGap - 1]
  } else {
    nowYear -= 1
    nowMonth = monthArray[(monthArray.length - monthGap + nowMonth) - 1]
  }
  const utcDate = new Date(nowYear, nowMonth, 2, 0, 0, 0).getTime();
  from3MonthsAgo = utcDate

  return from3MonthsAgo;
}