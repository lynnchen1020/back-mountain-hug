import orderRoutes from './order.js'
import mailRoutes from'./mail.js'
import holidayRoutes from'./holiday.js'


export const totalRoutes = [
  orderRoutes,
  mailRoutes,
  holidayRoutes
]

// export default totalRoutes;