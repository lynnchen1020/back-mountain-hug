import nodemailer from "nodemailer"
import handlebars from "handlebars"
import fs from "fs"
import path from "path"
import {dirname} from "path"
import { fileURLToPath } from 'url';
import { getCurrentDay } from '../helper/index.js';
import express from 'express';
const mailRoutes = express.Router();
import { insertEvent, deleteEvent } from "../functions/index.js";
const __dirname = dirname(fileURLToPath(import.meta.url));

const isDev = process.env.NODE_ENV === "local";

async function sendEmail(req, res) {
  try {
    const transport = nodemailer.createTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      // port: 465,
      secure: false,
      auth: {
        user: process.env.ACCOUNT,
        pass: process.env.PASS
      },
    });
    // calendar 預約日期
    const id = await insertCalendar(req.body);

    // if(req.body.start_date === '2023/10/7' || req.body.start_date === '2023/12/30') {
    //   console.log('連假第一天')
    //   res.send({
    //     status: true,
    //     event_id: id.event_id
    //   })
    //   return;
    // }

    let replacement = {
      order_id: req.body.order_id,
      area: getAreaTitle(req.body.area),
      name: req.body.name,
      phone: req.body.phone,
      email: req.body.email,
      start_date: `${req.body.start_date} (${getCurrentDay(req.body.start_date)})`,
      end_date: `${req.body.end_date} (${getCurrentDay(req.body.end_date)})`,
      nights: req.body.nights,
      prepaid_fee: req.body.prepaid_fee,
      total_fee: req.body.total_fee,
    }

    const campKeyOnly = {
      tents: req.body.tents
    }

    const innKeyOnly = {
      adults: req.body.adults,
      children: req.body.children,
      add_bed: req.body.add_bed,
      add_breakfast: req.body.add_breakfast
    }

    if(req.body.area !== 'inn') {
      replacement = Object.assign(replacement, campKeyOnly)
    }

    if(req.body.area === 'inn') {
      replacement = Object.assign(replacement, innKeyOnly)
    }

    // gmail 發送給客人預訂信 & 營主新訂單
    const mailTo = ['new-order-customers', 'new-order-boss']
    mailTo.forEach(async (type) => {
      const filePath = path.join(__dirname, `../email-templates/${type}.html`);
      const source = fs.readFileSync(filePath, 'utf-8').toString();
      const template = handlebars.compile(source);
      const htmlToSend = template(replacement);

      // Email主旨 & 內容
      var mailOptions = {
        from: '"山的抱抱"<mountainhugtw@gmail.com>',
        to: handleMailInfo(type, req.body).mailTo,
        subject: handleMailInfo(type, req.body).subject,
        html: htmlToSend
      }
      transport.sendMail(mailOptions, (err, info) => {
        if(err) {
          console.log('發送失敗，訊息:' + err);
        }
        console.log('發送成功');

        res.send({
          status: true,
          event_id: id.event_id
        })
      });
    })
  } catch (error) {
    console.log(error);
    res.send(error);
  }



  // const gmailOauth2 ="https://oauth2.googleapis.com/token?client_secret=" + process.env.CLINENTSECRET + "&grant_type=refresh_token&refresh_token=1%2F%2F04_a0s9UufkzoCgYIARAAGAQSNwF-L9IreObK5gXxpJoVvMGhmly9ceNVs07z6JGF--xdlaCw-K1TPB8JrGemivvHjKinDq1ouc0&client_id=" + process.env.CLINENTID

  // // fetch(gmailOauth2, {method: 'POST'}).then(response.json());
  // // const data = await response.json();

  // fetch(gmailOauth2, {method: 'POST'}).then(function(response) {
  //   // 直接轉成JSON格式
  //   return response.json()
  // }).then(function(obj) {
  //     // `obj`會是一個JavaScript物件
  //     const transporter = nodemailer.createTransport({
  //       host: 'smtp.gmail.com',
  //       port: 465,
  //       secure: true,
  //       auth: {
  //         type: "OAuth2",
  //         user: process.env.ACCOUNT,
  //         clientId: process.env.CLINENTID,
  //         clientSecret: process.env.CLINENTSECRET,
  //         refreshToken: process.env.REFRESHTOKEN,
  //         // accessToken: process.env.ACCESSTOKEN
  //         accessToken: obj.access_token,
  //       }
  //     });

  //     // calendar 預約日期
  //     insertCalendar(req.body);

  //     const replacement = {
  //       order_id: req.body.order_id,
  //       area: req.body.area,
  //       name: req.body.name,
  //       phone: req.body.phone,
  //       email: req.body.email,
  //       start_date: req.body.start_date,
  //       end_date: req.body.end_date,
  //       nights: req.body.nights,
  //       prepaid_fee: req.body.prepaid_fee,
  //       total_fee: req.body.total_fee,
  //       tents: req.body.tents
  //     }

  //     // gmail 發送給客人預訂信 & 營主新訂單
  //     const mailTo = ['new-order-customers', 'new-order-boss']
  //     mailTo.forEach(type => {
  //       const filePath = path.join(__dirname, `../email-templates/${type}.html`);
  //       const source = fs.readFileSync(filePath, 'utf-8').toString();
  //       const template = handlebars.compile(source);
  //       const htmlToSend = template(replacement);

  //       // Email主旨 & 內容
  //       var mailOption = {
  //         from: '"山的抱抱"<mountainhugtw@gmail.com>',
  //         to: handleMailInfo(type, req.body).mailTo,
  //         subject: handleMailInfo(type, req.body).subject,
  //         html: htmlToSend
  //       }

  //       transporter.sendMail(mailOption ,function(error, info) {
  //         if (error) {
  //           console.log('錯誤訊息:' + error);
  //           // res.redirect('/users/mailsend');
  //         }

  //         console.log('發送成功');
  //         // res.redirect('/api/mailsend');
  //         })

  //       res.send({
  //         status: true
  //       });
  //     })
  // }).catch(function(err) {
  //   // Error :(
  // })
}

async function reSendEmail(req, res) {
  try {
    const transport = nodemailer.createTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      // port: 465,
      secure: false,
      auth: {
        user: process.env.ACCOUNT,
        pass: process.env.PASS
      },
    });

    let replacement = {
      order_id: req.body.order_id,
      area: getAreaTitle(req.body.area),
      name: req.body.name,
      phone: req.body.phone,
      email: req.body.email,
      start_date: `${req.body.start_date} (${getCurrentDay(req.body.start_date)})`,
      end_date: `${req.body.end_date} (${getCurrentDay(req.body.end_date)})`,
      nights: req.body.nights,
      prepaid_fee: req.body.prepaid_fee,
      total_fee: req.body.total_fee,
    }

    const campKeyOnly = {
      tents: req.body.tents
    }

    const innKeyOnly = {
      adults: req.body.adults,
      children: req.body.children,
      add_bed: req.body.add_bed,
      add_breakfast: req.body.add_breakfast
    }

    if(req.body.area !== 'inn') {
      replacement = Object.assign(replacement, campKeyOnly)
    }

    if(req.body.area === 'inn') {
      replacement = Object.assign(replacement, innKeyOnly)
    }

    // gmail 發送給客人預訂信 & 營主新訂單
    const mailTo = ['new-order-customers']
    mailTo.forEach(async (type) => {
      const filePath = path.join(__dirname, `../email-templates/${type}.html`);
      const source = fs.readFileSync(filePath, 'utf-8').toString();
      const template = handlebars.compile(source);
      const htmlToSend = template(replacement);

      // Email主旨 & 內容
      var mailOptions = {
        from: '"山的抱抱"<mountainhugtw@gmail.com>',
        to: handleMailInfo(type, req.body).mailTo,
        subject: handleMailInfo(type, req.body).subject,
        html: htmlToSend
      }
      transport.sendMail(mailOptions, (err, info) => {
        if(err) {
          console.log('重新寄信，發送失敗，訊息:' + err);
        }
        console.log('重新寄信，發送成功');

        res.send({
          status: true
        })
      });
    })
  } catch (error) {
    console.log(error);
    res.send(error);
  }
}

async function sendHasPrepaidEmail(req, res) {
  try {
    const transport = nodemailer.createTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      // port: 465,
      secure: false,
      auth: {
        user: process.env.ACCOUNT,
        pass: process.env.PASS
      },
    });
    let replacement = {
      order_id: req.body.order_id,
      area: getAreaTitle(req.body.area),
      name: req.body.name,
      phone: req.body.phone,
      email: req.body.email,
      start_date: `${req.body.start_date} (${getCurrentDay(req.body.start_date)})`,
      end_date: `${req.body.end_date} (${getCurrentDay(req.body.end_date)})`,
      nights: req.body.nights,
      prepaid_fee: req.body.prepaid_fee,
      total_fee: req.body.total_fee,
    }

    const campKeyOnly = {
      tents: req.body.tents
    }

    const innKeyOnly = {
      adults: req.body.adults,
      children: req.body.children,
      add_bed: req.body.add_bed,
      add_breakfast: req.body.add_breakfast
    }

    if(req.body.area !== 'inn') {
      replacement = Object.assign(replacement, campKeyOnly)
    }

    if(req.body.area === 'inn') {
      replacement = Object.assign(replacement, innKeyOnly)
    }

    const filePath = path.join(__dirname, `../email-templates/fee-paid-reply.html`);
    const source = fs.readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);
    const htmlToSend = template(replacement);

    const mailOptions = {
      from: '"山的抱抱"<mountainhugtw@gmail.com>',
      to: handleMailInfo('prepaid-to-customers', req.body).mailTo,
      subject: handleMailInfo('prepaid-to-customers', req.body).subject,
      html: htmlToSend
    };
    transport.sendMail(mailOptions, (err, info) => {
      if(err) {
        console.log('發送失敗，訊息:' + err);
      }
      console.log('發送成功');

      res.send({
        status: true
      })
    });
  } catch (error) {
    console.log(error);
    res.send(error);
  }

  // const gmailOauth2 ="https://oauth2.googleapis.com/token?client_secret=" + process.env.CLINENTSECRET + "&grant_type=refresh_token&refresh_token=1//04nTZheXXsLz3CgYIARAAGAQSNwF-L9Iri-iCvVD0a7gTntGhLCx_xE1grIHkKynsdRKZ8Eq8m7ANssrSXWQ56Xhj8dIC4_pJQqA&client_id=" + process.env.CLINENTID

  // fetch(gmailOauth2, {method: 'POST'}).then(function(response) {
  //   // 直接轉成JSON格式
  //   return response.json()
  // }).then(function(obj) {
  //     console.log('obj', obj)
  //     // `obj`會是一個JavaScript物件
  //     const transporter = nodemailer.createTransport({
  //       host: 'smtp.gmail.com',
  //       port: 465,
  //       secure: true,
  //       auth: {
  //         type: "OAuth2",
  //         user: process.env.ACCOUNT,
  //         clientId: process.env.CLINENTID,
  //         clientSecret: process.env.CLINENTSECRET,
  //         refreshToken: process.env.REFRESHTOKEN,
  //         // accessToken: process.env.ACCESSTOKEN
  //         accessToken: obj.access_token,
  //       }
  //     });

  //     const replacement = {
  //       order_id: req.body.order_id,
  //       area: req.body.area,
  //       name: req.body.name,
  //       phone: req.body.phone,
  //       email: req.body.email,
  //       start_date: req.body.start_date,
  //       end_date: req.body.end_date,
  //       nights: req.body.nights,
  //       prepaid_fee: req.body.prepaid_fee,
  //       total_fee: req.body.total_fee,
  //       tents: req.body.tents
  //     }

  //     const filePath = path.join(__dirname, `../email-templates/fee-paid-reply.html`);
  //     const source = fs.readFileSync(filePath, 'utf-8').toString();
  //     const template = handlebars.compile(source);
  //     const htmlToSend = template(replacement);

  //     // Email主旨 & 內容
  //     var mailOption = {
  //       from: '"山的抱抱"<mountainhugtw@gmail.com>',
  //       to: handleMailInfo('prepaid-to-customers', req.body).mailTo,
  //       subject: handleMailInfo('prepaid-to-customers', req.body).subject,
  //       html: htmlToSend
  //     }

  //     transporter.sendMail(mailOption ,function(error, info) {
  //       if (error) {
  //         console.log('錯誤訊息:' + error);
  //         // res.redirect('/users/mailsend');
  //         res.send({
  //           status: false
  //         });
  //         return
  //       }

  //       console.log('發送成功');
  //       // res.redirect('/api/mailsend');
  //       res.send({
  //         status: true
  //       });
  //     })
  // }).catch(function(err) {
  //   // Error :(
  // })
}

function handleMailInfo(type, order) {
  const isCustomers = type === 'new-order-customers' || type === 'prepaid-to-customers';
  const isDevMail = process.env.NODE_ENV === "local" ? process.env.CALENDAR_ID : 'mountainhugtw@gmail.com'

  switch(type) {
    case 'new-order-customers':
      return {
        subject: `【${getAreaTitle(order.area)}】- 已收到您的訂位需求`,
        mailTo: isCustomers ? order.email : isDevMail
      }
    case 'new-order-boss':
      return {
        subject: `【${getAreaTitle(order.area)}】新訂單-來自${order.name}的預訂 ${order.order_id}`,
        mailTo: isCustomers ? order.email : isDevMail
      }
    case 'prepaid-to-customers':
      return {
        subject: `【${getAreaTitle(order.area)}】- 您已完成訂位`,
        mailTo: isCustomers ? order.email : isDevMail
      }
    default:
      return {
        subject: "",
        mailTo: ""
      }
  }

  // const mailConfig = {
  //   subject,
  //   mailTo
  // }
  // return mailConfig
}

function getAreaTitle(area) {
  switch(area) {
    case 'camp_a':
      return '五葉松 A區'
    case 'camp_b':
      return '落羽松 L區'
    case 'camp_c':
      return 'C_雨棚區'
    case 'camp_d':
      return 'D_XX區'
    case 'inn':
      return '山中宿屋'
    default:
      return ""
  }
}

function getCalendarID(area) {
  switch(area) {
    // a本營區
    case 'camp_a':
      return isDev ? '' : '40a082cd424ea569ce2594b907cf737fb003de94de99de93d7f7df6cb042b3a6@group.calendar.google.com'
    // b落羽松
    case 'camp_b':
      return isDev ? '' : '812c390813c3fd63280064c77ed7b0c470f9302d5445e51fb87cb19127026b0e@group.calendar.google.com'
    // c雨棚區
    case 'camp_c':
      return isDev ? '' : '38906e06d5db1511085fa296240ca5976fff884387b72277589618e431cef803@group.calendar.google.com'
    // d xx區
    case 'camp_d':
      return isDev ? '' : '4f60fc445512e07304e89cd8c67a0ec48c9763b71e7c0967526ec005dfced1e0@group.calendar.google.com'
    // e山中宿屋
    case 'inn':
      return isDev ? '' : 'aa3fbe3cc63e8a2d4c2c054c4c79cee4b82da38682e1b26bf13cffcc17ce639c@group.calendar.google.com'
    default:
      return ""
  }
}

function addZero(num) {
  const result = Number(num) < 10 ? `0${num}` : String(num)
  return result
}

function insertCalendar(order) {
  // 發送 google calendar start
  const calendarID = getCalendarID(order.area)
  // const start = new Date(order.start_date)
  // const end = new Date(order.end_date)

  const start = order.start_date.split('/')
  const end = order.end_date.split('/')
  const newStartStr = `${start[0]}-${addZero(start[1])}-${addZero(start[2])}`
  const newEndStr = `${end[0]}-${addZero(end[1])}-${addZero(end[2])}`


  const newStart = new Date(`${String(newStartStr)}T00:00:00+08:00`)
  const newEnd = new Date(`${String(newEndStr)}T00:00:00+08:00`)

  const isInn = order.area == 'inn' ?
  `大人: ${order.adults} 位
    小孩: ${order.children} 位
    加床: ${order.add_bed ? order.add_bed : 0} 張` : `帳數: ${order.tents} 帳`
  let event = {
    'summary': `【${getAreaTitle(order.area)}】 ${order.name}`,
    'description':`
    ===== 訂單內容 =====
    ${isInn}
    總共入住: ${order.nights} 晚
    小計金額: ${order.total_fee} 元
    訂金: ${order.prepaid_fee} 元

    ===== 預訂人資訊 =====
    姓名: ${order.name}
    聯絡電話: ${order.phone}
    Email: ${order.email}
    訂單編號: ${order.order_id}
    `,
    'start': {
      'dateTime': newStart,
        // 'dateTime': "2022-12-24T00:00:00.000Z",
    },
    'end': {
      'dateTime': newEnd,
        // 'dateTime': endDT,
    }
  };

  // if(process.env.NODE_ENV === 'local') {
  //   console.log('實際送出的calendar內容', event, calendarID)
  //   return
  // }

  return insertEvent(event, calendarID)
  // 發送 google calendar end
}

// SendEmail Api
mailRoutes.post('/mailsend',function(req, res, next) {
  sendEmail(req, res)
});

// 重新寄信給客人
mailRoutes.post('/resend_mail',function(req, res, next) {
  reSendEmail(req, res)
});

mailRoutes.post('/mailsend_prepaid',function(req, res, next) {
  sendHasPrepaidEmail(req, res)
});

mailRoutes.get('/calendar',function() {
  insertCalendar();
});
mailRoutes.post('/calendar',function(req, res) {
  deleteEvent(getCalendarID(req.body.area), req.body.event_id);
});

export default mailRoutes;