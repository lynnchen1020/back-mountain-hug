import express from 'express';
import Model from '../models/index.js';
import { getHalfYearOrder } from '../helper/index.js';
const holidayRoutes = express.Router();

var from3MonthsAgo = getHalfYearOrder();

// Backend Get Holidayss
holidayRoutes.get('/back/holiday', async (req, res) => {
  try {
    const data = await Model['holiday'].find({holiday_ts: {$gte: from3MonthsAgo}}, {_id: 0}, {new: true}).sort({holiday_ts: 1})
    res.status(200).json(data);

  } catch(err) {
    res.status(500).json({
      status: false,
      message: err.message
    });
  }
});

// Backend Send Holidays
holidayRoutes.post('/back/holiday', async (req, res) => {
  try {
    const data = new Model['holiday']({
      holiday_ts: req.body.holiday_ts, //[{holiday_ts: 1670342400000, created_time: 1988292938}]
      alias: req.body.alias,
      created_time: new Date().getTime()
    })
    await data.save();

    res.status(200).json({
      status: true,
      message: 'ok',
    })

  } catch(err) {
    res.status(500).json({
      status: false,
      message: err.message
    });
  }
});

// Backend Delete Holiday
holidayRoutes.post('/back/holiday/delete', async (req, res) => {
  try {
    await Model['holiday'].findOneAndDelete({
      holiday_ts: req.body.holiday_ts
    })

    res.status(200).json({
      status: true,
      message: "data deleted!"
    })
  } catch(err) {
    res.status(500).json({
      status: false,
      message: err.message
    });
  }
});

export default holidayRoutes