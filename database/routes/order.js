import express from 'express';
import Model from '../models/index.js';
import _ from 'lodash'
import { getHalfYearOrder } from '../helper/index.js';
const orderRoutes = express.Router();
// import nodemailer from 'nodemailer';
// require('dotenv').config()
// import {} from 'dotenv/config'

// 為減輕查詢效能，前端顯示前 3 個月 Orders
var from3MonthsAgo = getHalfYearOrder();


orderRoutes.get('/ping',(req, res) => {
  res.send('pong!');
});

const req_created_time = {
  'camp_a': {},
  'camp_b': {},
  'inn': {}
}

// Frontend Check If Order Duplicate
orderRoutes.get('/order/:orderType/:orderID', async (req, res) => {
  const sec = 100000
  const dayTS = 86400000
  const created_time = req.query.timestamp
  const start_date = new Date(req.query.startDate).getTime() // ['1928384738']
  const nights = Number(req.query.nights);
  let total_stay_dates = [];
  for(let i = 0; i < nights; i++) {
    total_stay_dates.push((start_date + (i * dayTS)).toString())
  }

  // console.log('nights', nights)
  // console.log('total_stay_dates', total_stay_dates)
  // console.log('start_date', start_date)
  const area = req.params.orderType

  // 計算每次進來的 request 重複日期的秒數差異
  const diff = {
    'camp_a': {},
    'camp_b': {},
    'inn': {},
  }

  // 檢查預定日期是否已經存在
  const booked_dates_record = _.intersection(Object.keys(req_created_time[area]), total_stay_dates)
  // console.log('1111 total_stay_dates 送出後，訂進來的日期', total_stay_dates)
  // console.log('2222 req_created_time 記錄到已訂進去的日期的TS', req_created_time)
  // console.log('3333 booked_dates_record 找出訂進來跟既有紀錄的交集日期', booked_dates_record)

  // 把已存在的日期往後加上 created time
  if(booked_dates_record.length > 0) {
    total_stay_dates.forEach(start_date => {
      if(!Object.keys(req_created_time[area]).includes(start_date)) {
        req_created_time[area][start_date] = []
      }
      req_created_time[area][start_date].push(created_time)
    })
    // console.log('2 req_created_time', req_created_time)

    // 找出有超過2次 request 的重複日期
    const isOverTwoTimeStampsDates = total_stay_dates.map(start_date => {
        return req_created_time[area][start_date].length > 2
    }).includes(true)

    // console.log('3 isOverTwoTimeStampsDates', isOverTwoTimeStampsDates)

    // const conflictDateIndex = isOverTwoTimeStampsDates.indexOf(true);

    // console.log('4 conflictDateIndex', conflictDateIndex)

    if(isOverTwoTimeStampsDates) {
      diff[area][booked_dates_record[0]] =
      req_created_time[area][booked_dates_record[0]][req_created_time[area][booked_dates_record[0]].length-1] - req_created_time[area][booked_dates_record[0]][req_created_time[area][booked_dates_record[0]].length-2]

      if(booked_dates_record.length === 2) {
        diff[area][booked_dates_record[1]] =
        req_created_time[area][booked_dates_record[1]][req_created_time[area][booked_dates_record[1]].length-1] - req_created_time[area][booked_dates_record[1]][req_created_time[area][booked_dates_record[1]].length-2]
        // console.log('aaa 有進來 booked_dates_record.length > 1',diff[area][booked_dates_record[1]])
      }

      if(booked_dates_record.length === 3) {
        diff[area][booked_dates_record[2]] =
        req_created_time[area][booked_dates_record[2]][req_created_time[area][booked_dates_record[2]].length-1] - req_created_time[area][booked_dates_record[2]][req_created_time[area][booked_dates_record[2]].length-2]
        // console.log('aaa 有進來 booked_dates_record.length > 1',diff[area][booked_dates_record[2]])
      }

      // console.log('5555 有紀錄的 diff', diff)
      if(diff[area][booked_dates_record[0]] < sec || diff[area][booked_dates_record[1]] < sec || diff[area][booked_dates_record[2]] < sec) {
        // console.log('低於6秒', req_created_time)
        // 下個 req 低於 3 秒
        res.status(200).json({
          status: 'failed',
          msg: 'busy network'
        })
        return;
      }

       res.status(200).json({
         status: 'success',
         msg: 'saved!'
       })
      //  console.log('超過3秒', req_created_time)

    } else {
      diff[area][booked_dates_record[0]] = req_created_time[area][booked_dates_record[0]][req_created_time[area][booked_dates_record[0]].length-1] - req_created_time[area][booked_dates_record[0]][0]

      if(booked_dates_record.length === 2) {
        diff[area][booked_dates_record[1]] = req_created_time[area][booked_dates_record[1]][req_created_time[area][booked_dates_record[1]].length-1] - req_created_time[area][booked_dates_record[1]][0]

        // console.log('bbb 有進來 booked_dates_record.length > 1',diff[area][booked_dates_record[1]])
      }

      if(booked_dates_record.length === 3) {
        diff[area][booked_dates_record[2]] = req_created_time[area][booked_dates_record[2]][req_created_time[area][booked_dates_record[2]].length-1] - req_created_time[area][booked_dates_record[2]][0]

        // console.log('bbb 有進來 booked_dates_record.length > 1',diff[area][booked_dates_record[1]])
      }

      // console.log('5555 未有紀錄的 diff', diff)
      if(diff[area][booked_dates_record[0]] < sec || diff[area][booked_dates_record[1]] < sec || diff[area][booked_dates_record[2]] < sec) {
        // console.log('低於6秒', req_created_time)
        // 下個 req 低於 3 秒
        res.status(200).json({
          status: 'failed',
          msg: 'busy network'
        })
        return;
      }

      // console.log('已超過2秒才送出，應該要從前端檢查是否重覆訂單')
      res.status(200).json({
        status: 'success',
        msg: 'saved!'
      })

      // console.log('else req_created_time', req_created_time)
    }
  } else {
    total_stay_dates.forEach(start_date => {
      req_created_time[area][start_date] = []
      req_created_time[area][start_date].push(created_time)
    })
    // console.log('0000 第一次紀錄', req_created_time)

    res.status(200).json({
      status: 'success',
      msg: 'saved!'
    })
  }




  // const data = await Model[req.params.orderType].find({order_id: req.params.orderID});
  //   if(data.length > 0) {
  //     res.status(200).json({
  //       status: "success",
  //       msg: "Duplicate Data"
  //     })
  //     return
  //   }

  //   res.status(200).json({
  //     status: "failed",
  //     msg: "No Duplicate Data"
  //   })
})

// Frontend Send Order
orderRoutes.post('/order/:orderType', async (req, res) => {
  if(req.params.orderType.includes('camp')) {
    response(campOrders(req), res);
    return;
  }

  if(req.params.orderType === 'inn') {
    response(innOrders(req), res)

    return;
  }
})

async function response(data, res) {
  try {


    await data.save();
    // Post 成功後回傳給前端的資料
    res.status(200).json({
      status: 'success',
      msg: ''
    })
  }
  catch (error) {
    res.status(400).json({ message: error.message })
  }
}

// Frontend Get Orders
orderRoutes.get('/order/:orderType', async (req, res) => {
  try {
      const data = await Model[req.params.orderType].find({is_canceled_order: false, timestamp_start_date: {$gte: from3MonthsAgo}}, {
        order_id: 1,
        area: 1,
        start_date: 1,
        end_date: 1,
        tents_per_day: 1,
        nights: 1,
        order_source: 1,
        is_canceled_order: 1,
        created_time: 1,
        timestamp_start_date: 1
      });
      res.json(data)
    }
    catch (error) {
        res.status(500).json({ message: error.message })
    }
})

// Backend Get Order
orderRoutes.get('/back/order/:orderType', async (req, res) => {
  try {
    const data = await Model[req.params.orderType].find({timestamp_start_date: {$gte: from3MonthsAgo}}, {
      _id: 0
    });
    res.json(data)
  }
  catch (error) {
    // console.log('from3MonthsAgo', from3MonthsAgo)
    res.status(500).json({ message: error.message })
  }
})
// Backend Update Order
orderRoutes.patch('/back/order/:orderType/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const updatedData = req.body;
    const option =  { new: true };

    await Model[req.params.orderType].findOneAndUpdate(
      {order_id: id}, updatedData, option
    )

    res.send({
      id,
      area: req.params.orderType,
      is_success: true
    })
  }
  catch (error) {
    res.status(500).json({ message: error.message })
  }
})
// Admin API end


function innOrders(req) {
  const data = new Model[req.params.orderType]({
    order_id: req.body.order_id,
    area: req.body.area,
    name: req.body.name,
    phone: req.body.phone,
    email: req.body.email,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    nights: req.body.nights,
    timestamp_start_date: req.body.timestamp_start_date,
    order_details: req.body.order_details,
    tents_per_day: req.body.tents_per_day, //[1670342400000,1670342400000,1670428800000,1670428800000] = [12/7, 12/7, 12/8, 12/8]
    prepaid_fee: req.body.prepaid_fee,
    is_prepaid: req.body.is_prepaid,
    rest_fee: req.body.rest_fee,
    is_restpaid: req.body.is_restpaid,
    total_fee: req.body.total_fee,
    order_source: req.body.order_source,
    is_canceled_order: false,
    event_id: req.body.event_id,
    tents: req.body.tents,
    adults: req.body.adults,
    children: req.body.children,
    add_breakfast: req.body.add_breakfast,
    add_bed: req.body.add_bed,
    // is_night_going: req.body.is_night_going,
    created_time: req.body.created_time
  })
  return data;
}
function campOrders(req) {
  const data = new Model[req.params.orderType]({
    order_id: req.body.order_id,
    area: req.body.area,
    name: req.body.name,
    phone: req.body.phone,
    email: req.body.email,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    nights: req.body.nights,
    timestamp_start_date: req.body.timestamp_start_date,
    tents_per_day: req.body.tents_per_day, //[1670342400000,1670342400000,1670428800000,1670428800000] = [12/7, 12/7, 12/8, 12/8]
    order_details: req.body.order_details,
    prepaid_fee: req.body.prepaid_fee,
    is_prepaid: req.body.is_prepaid,
    rest_fee: req.body.rest_fee,
    is_restpaid: req.body.is_restpaid,
    total_fee: req.body.total_fee,
    order_source: req.body.order_source,
    is_canceled_order: false,
    event_id: req.body.event_id,
    tents: req.body.tents,
    is_night_going: req.body.is_night_going,
    created_time: req.body.created_time
  })

  return data;
}

export default orderRoutes;


//Get by ID Method
// router.get('/getOne/:id', async (req, res) => {
//     try {
//         const data = await Model.findById(req.params.id);
//         res.json(data)
//     }
//     catch (error) {
//         res.status(500).json({ message: error.message })
//     }
// })

//Update by ID Method
// router.patch('/update/:id', async (req, res) => {
//     try {
//         const id = req.params.id;
//         const updatedData = req.body;
//         const options = { new: true };

//         const result = await Model.findByIdAndUpdate(
//             id, updatedData, options
//         )

//         res.send(result)
//     }
//     catch (error) {
//         res.status(500).json({ message: error.message })
//     }
// })

//Delete by ID Method
// router.delete('/delete/:id', async (req, res) => {
//     try {
//         const id = req.params.id;
//         const data = await Model.findByIdAndDelete(id)
//         res.send(`Document with ${data.name} has been deleted..`)
//     }
//     catch (error) {
//         res.status(400).json({ message: error.message })
//     }
// })